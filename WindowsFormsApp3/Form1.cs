﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        Bitmap bmp;
        WEngine we;
        int fps = 0;
        public Form1()
        {
            InitializeComponent();
        }

        int cntr = 0;

        mesh cubeMesh;
        private void Form1_Load(object sender, EventArgs e)
        {
            we = new WEngine(screen.Width, screen.Height);
            bmp = new Bitmap(this.screen.Width, this.screen.Height);
            screen.Image = bmp;
            cubeMesh = new mesh();

            cubeMesh.loadFromOBJ("parrot.obj");

            //// FRONT
            //cubeMesh.triangles.Add(new Triangle(new vec3d(0, 0, 0), new vec3d(0, 1, 0), new vec3d(1, 1, 0)));
            //cubeMesh.triangles.Add(new Triangle(new vec3d(0, 0, 0), new vec3d(1, 1, 0), new vec3d(1, 0, 0)));

            //// RIGHT
            //cubeMesh.triangles.Add(new Triangle(new vec3d(1, 0, 0), new vec3d(1, 1, 0), new vec3d(1, 1, 1)));
            //cubeMesh.triangles.Add(new Triangle(new vec3d(1, 0, 0), new vec3d(1, 1, 1), new vec3d(1, 0, 1)));

            //// BACK
            //cubeMesh.triangles.Add(new Triangle(new vec3d(1, 0, 1), new vec3d(1, 1, 1), new vec3d(0, 1, 1)));
            //cubeMesh.triangles.Add(new Triangle(new vec3d(1, 0, 1), new vec3d(0, 1, 1), new vec3d(0, 0, 1)));

            //// LEFT
            //cubeMesh.triangles.Add(new Triangle(new vec3d(0, 0, 1), new vec3d(0, 1, 1), new vec3d(0, 1, 0)));
            //cubeMesh.triangles.Add(new Triangle(new vec3d(0, 0, 1), new vec3d(0, 1, 0), new vec3d(0, 0, 0)));

            //// TOP
            //cubeMesh.triangles.Add(new Triangle(new vec3d(0, 1, 0), new vec3d(0, 1, 1), new vec3d(1, 1, 1)));
            //cubeMesh.triangles.Add(new Triangle(new vec3d(0, 1, 0), new vec3d(1, 1, 1), new vec3d(1, 1, 0)));

            //// BOT
            //cubeMesh.triangles.Add(new Triangle(new vec3d(1, 0, 1), new vec3d(0, 0, 1), new vec3d(0, 0, 0)));
            //cubeMesh.triangles.Add(new Triangle(new vec3d(1, 0, 1), new vec3d(0, 0, 0), new vec3d(1, 0, 0)));

            // translation z += 3

        }

        private void frameRate_Tick(object sender, EventArgs e)
        {
            // clear
            var g = Graphics.FromImage(bmp);
            g.Clear(Color.FromArgb(255, 40, 34, 49));

            // draw
            foreach(var tr in cubeMesh.triangles)
            {
                vec3d a = new vec3d();
                vec3d b = new vec3d();
                vec3d c = new vec3d();

                we.rotateDegree(tr.points[0], a, (double)cntr/2, WEngine.axis.Y);
                we.rotateDegree(tr.points[1], b, (double)cntr/2, WEngine.axis.Y);
                we.rotateDegree(tr.points[2], c, (double)cntr/2, WEngine.axis.Y);

                Triangle trRotatedX = new Triangle(a, b, c);

                a = new vec3d();
                b = new vec3d();
                c = new vec3d();

                we.rotateDegree(trRotatedX.points[0], a, 180, WEngine.axis.Z);
                we.rotateDegree(trRotatedX.points[1], b, 180, WEngine.axis.Z);
                we.rotateDegree(trRotatedX.points[2], c, 180, WEngine.axis.Z);

                Triangle trFinal = new Triangle(a, b, c);

                for (int i = 0; i < 3; i++)
                {
                    trFinal.points[i].z += 35;
                    trFinal.points[i].y += 10;
                }

                a = new vec3d();
                b = new vec3d();
                c = new vec3d();

                // line A
                a.x = trFinal.points[1].x - trFinal.points[0].x;
                a.y = trFinal.points[1].y - trFinal.points[0].y;
                a.z = trFinal.points[1].z - trFinal.points[0].z;

                // line B
                b.x = trFinal.points[2].x - trFinal.points[0].x;
                b.y = trFinal.points[2].y - trFinal.points[0].y;
                b.z = trFinal.points[2].z - trFinal.points[0].z;

                // C (normal) = cross-product of A and B
                c.x = a.y * b.z - a.z * b.y;
                c.y = a.z * b.x - a.x * b.z;
                c.z = a.x * b.y - a.y * b.x;

                // normalize (set len between 0 and 1)
                double ln = Math.Sqrt(c.x * c.x + c.y * c.y + c.z * c.z);
                c.x /= ln;
                c.y /= ln;
                c.z /= ln;

                if (c.x * (trFinal.points[0].x - we.camera.x) +
                    c.y * (trFinal.points[0].y - we.camera.y) +
                    c.z * (trFinal.points[0].z - we.camera.z) < 0.0)
                {

                    // lighting 
                    vec3d light_dir = new vec3d(0, 0, -1);
                    double light_len = Math.Sqrt(light_dir.x * light_dir.x + light_dir.y * light_dir.y + light_dir.z * light_dir.z);
                    light_dir.x /= light_len;
                    light_dir.y /= light_len;
                    light_dir.z /= light_len;

                    double lightDP = c.x * light_dir.x + c.y * light_dir.y + c.z * light_dir.z;

                    a = new vec3d();
                    b = new vec3d();
                    c = new vec3d();
                    // 3D -> 2D
                    we.projectVector(trFinal.points[0], a);
                    we.projectVector(trFinal.points[1], b);
                    we.projectVector(trFinal.points[2], c);


                    // center
                    a.x += 1; a.y += 1;
                    b.x += 1; b.y += 1;
                    c.x += 1; c.y += 1;

                    // scale
                    a.x *= 0.5 * screen.Width;
                    a.y *= 0.5 * screen.Height;
                    b.x *= 0.5 * screen.Width;
                    b.y *= 0.5 * screen.Height;
                    c.x *= 0.5 * screen.Width;
                    c.y *= 0.5 * screen.Height;

                    Triangle tproj = new Triangle(a, b, c);

                    //tproj.fillTriangle(bmp, lightDP);
                    tproj.frameTriangle(bmp);
                }
            }

            // set
            screen.Image = bmp;
            fps++;
        }

        private void speed_Tick(object sender, EventArgs e)
        {
            cntr+=10;
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            we = new WEngine(screen.Width, screen.Height);
            bmp = new Bitmap(this.screen.Width, this.screen.Height);
            fpsLabel.Left = screen.Width - fpsLabel.Size.Width;
            screen.Image = bmp;
        }

        private void fpsCounter_Tick(object sender, EventArgs e)
        {
            fpsLabel.Text = "FPS: " + (fps*10).ToString();
            fpsLabel.Left = screen.Width - fpsLabel.Size.Width;
            fps = 0;
        }
    }

   
}
