﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Globalization;

namespace WindowsFormsApp3
{
    public class WEngine
    {
        public enum axis
        {
            X,
            Y,
            Z
        }

        public vec3d camera { get; set; }

        double near;
        double renderDistance;
        double fov;
        double aspectRatio;
        double fovCtg;
        double[,] matProj = new double[4, 4];
        public WEngine(double width, double height)
        {
            near = 0.1;
            renderDistance = 5000.0;
            fov = 90.0;
            aspectRatio = height / width;
            fovCtg = 1.0 / Math.Tan(fov * 0.5 * Math.PI / 180.0);

            matProj[0, 0] = aspectRatio * fovCtg;
            matProj[1, 1] = fovCtg;
            matProj[2, 2] = renderDistance / (renderDistance - near);
            matProj[3, 2] = (-renderDistance * near) / (renderDistance - near);
            matProj[2, 3] = 1.0;
            matProj[3, 3] = 0.0;
            camera = new vec3d();
        }
        void matVecMultiply(vec3d v1, vec3d v2, double[,] mat)
        {
            v2.x = v1.x * mat[0, 0] + v1.y * mat[1, 0] + v1.z * mat[2, 0] + mat[3, 0];
            v2.y = v1.x * mat[0, 1] + v1.y * mat[1, 1] + v1.z * mat[2, 1] + mat[3, 1];
            v2.z = v1.x * mat[0, 2] + v1.y * mat[1, 2] + v1.z * mat[2, 2] + mat[3, 2];
            double w = v1.x * mat[0, 3] + v1.y * mat[1, 3] + v1.z * mat[2, 3] + mat[3, 3];
            if (w != 0.0)
            {
                v2.x /= w;
                v2.y /= w;
                v2.z /= w;
            }
        }

        public void projectVector(vec3d p1, vec3d p2)
        {
            matVecMultiply(p1, p2, matProj);
        }

        public void rotateDegree(vec3d p1, vec3d p2, double angle, axis a)
        {
            angle *= Math.PI / 180;
            double[,] matRot = new double[4, 4];
            if (a == axis.X)
            {
                matRot[0, 0] = 1;
                matRot[1, 1] = Math.Cos(angle);
                matRot[1, 2] = Math.Sin(angle);
                matRot[2, 1] = -Math.Sin(angle);
                matRot[2, 2] = Math.Cos(angle);
                matRot[3, 3] = 1;
            }
            else if (a == axis.Y)
            {

                matRot[0, 0] = Math.Cos(angle);
                matRot[1, 1] = 1;
                matRot[0, 2] = -Math.Sin(angle);
                matRot[2, 0] = Math.Sin(angle);
                matRot[2, 2] = Math.Cos(angle);
                matRot[3, 3] = 1;
            }
            else if (a == axis.Z)
            {
                matRot[0, 0] = Math.Cos(angle);
                matRot[0, 1] = Math.Sin(angle);
                matRot[1, 0] = -Math.Sin(angle);
                matRot[1, 1] = Math.Cos(angle);
                matRot[2, 2] = 1;
                matRot[3, 3] = 1;
            }
            matVecMultiply(p1, p2, matRot);
        }

    }
    public class vec3d
    {
        public static void draw_line(Bitmap bitm, vec3d p1, vec3d p2)
        {
            Pen p = new Pen(Color.FromArgb(255, 0, 0, 0), 1);

            using (var g = Graphics.FromImage(bitm))
            {
                g.DrawLine(p, (int)p1.x, (int)p1.y, (int)p2.x, (int)p2.y);
            }
        }

        public double x { get; set; }
        public double y { get; set; }
        public double z { get; set; }
        public vec3d(double x = 0, double y = 0, double z = 0)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

    public class Triangle
    {
        public vec3d[] points;

        private Point[] points2d;
        public Triangle(vec3d p1, vec3d p2, vec3d p3)
        {
            points = new vec3d[3];
            points2d = new Point[3];
            this.updatePosition(p1, p2, p3);
        }
        public void updatePosition(vec3d p1, vec3d p2, vec3d p3)
        {
            points[0] = p1;
            points[1] = p2;
            points[2] = p3;
            for (int i = 0; i < 3; i++)
            {
                points2d[i].X = (int)points[i].x;
                points2d[i].Y = (int)points[i].y;
            }

        }

        public void frameTriangle(Bitmap bitm)
        {
            vec3d.draw_line(bitm, points[0], points[1]);
            vec3d.draw_line(bitm, points[1], points[2]);
            vec3d.draw_line(bitm, points[0], points[2]);
        }

        public void fillTriangle(Bitmap bitm, double lightning)
        {
            int val = (int)(255 * (lightning + 1) / 2);
            SolidBrush p = new SolidBrush(Color.FromArgb(255, val, val, val));
            using (var g = Graphics.FromImage(bitm))
            {
                g.FillPolygon(p, points2d);
            }
        }
    }

    public class mesh
    {
        public static double GetDouble(string value, double defaultValue)
        {
            double result;

            //Try parsing in the current culture
            if (!double.TryParse(value, System.Globalization.NumberStyles.Any, CultureInfo.CurrentCulture, out result) &&
                //Then try in US english
                !double.TryParse(value, System.Globalization.NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"), out result) &&
                //Then in neutral language
                !double.TryParse(value, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                result = defaultValue;
            }

            return result;
        }


        public List<Triangle> triangles;
        public mesh()
        {
            triangles = new List<Triangle>();
        }

        public bool loadFromOBJ(string filePath)
        {
            if (File.Exists(filePath))
            {
                using (StreamReader file = new StreamReader(filePath))
                {
                    string line;
                    List<vec3d> points = new List<vec3d>();
                    List<vec3d> normals = new List<vec3d>();
                    while ((line = file.ReadLine()) != null)
                    {
                        string[] words = line.Split(' ');
                        if (words[0] == "v")
                        {
                            points.Add(new vec3d(GetDouble(words[1], 0), GetDouble(words[2], 0), GetDouble(words[3], 0)));
                        }
                        if (words[0] == "vn")
                        {
                            normals.Add(new vec3d(GetDouble(words[1], 0), GetDouble(words[2], 0), GetDouble(words[3], 0)));
                        }
                        else if (words[0] == "f")
                        {
                            string[] finalval;
                            int[] trPoints = new int[3];
                            finalval = words[1].Split('/');
                            trPoints[0] = Convert.ToInt32(finalval[0]);

                            finalval = words[2].Split('/');
                            trPoints[1] = Convert.ToInt32(finalval[0]);

                            finalval = words[3].Split('/');
                            trPoints[2] = Convert.ToInt32(finalval[0]);
                            triangles.Add(new Triangle(points[trPoints[0] - 1], points[trPoints[1] - 1], points[trPoints[2] - 1]));
                        }
                    }
                    file.Close();
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
