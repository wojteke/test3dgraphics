﻿namespace WindowsFormsApp3
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.screen = new System.Windows.Forms.PictureBox();
            this.frameRate = new System.Windows.Forms.Timer(this.components);
            this.speed = new System.Windows.Forms.Timer(this.components);
            this.fpsCounter = new System.Windows.Forms.Timer(this.components);
            this.fpsLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.screen)).BeginInit();
            this.SuspendLayout();
            // 
            // screen
            // 
            this.screen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.screen.Location = new System.Drawing.Point(0, 0);
            this.screen.Name = "screen";
            this.screen.Size = new System.Drawing.Size(962, 595);
            this.screen.TabIndex = 0;
            this.screen.TabStop = false;
            // 
            // frameRate
            // 
            this.frameRate.Enabled = true;
            this.frameRate.Interval = 10;
            this.frameRate.Tick += new System.EventHandler(this.frameRate_Tick);
            // 
            // speed
            // 
            this.speed.Enabled = true;
            this.speed.Interval = 10;
            this.speed.Tick += new System.EventHandler(this.speed_Tick);
            // 
            // fpsCounter
            // 
            this.fpsCounter.Enabled = true;
            this.fpsCounter.Tick += new System.EventHandler(this.fpsCounter_Tick);
            // 
            // fpsLabel
            // 
            this.fpsLabel.AutoSize = true;
            this.fpsLabel.ForeColor = System.Drawing.Color.White;
            this.fpsLabel.Location = new System.Drawing.Point(912, 9);
            this.fpsLabel.Name = "fpsLabel";
            this.fpsLabel.Size = new System.Drawing.Size(50, 17);
            this.fpsLabel.TabIndex = 1;
            this.fpsLabel.Text = "FPS: 0";
            this.fpsLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(34)))), ((int)(((byte)(49)))));
            this.ClientSize = new System.Drawing.Size(962, 595);
            this.Controls.Add(this.fpsLabel);
            this.Controls.Add(this.screen);
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "Form1";
            this.Text = "WEngineTest";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.screen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox screen;
        private System.Windows.Forms.Timer frameRate;
        private System.Windows.Forms.Timer speed;
        private System.Windows.Forms.Timer fpsCounter;
        private System.Windows.Forms.Label fpsLabel;
    }
}

